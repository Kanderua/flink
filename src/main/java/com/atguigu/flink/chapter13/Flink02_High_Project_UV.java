package com.atguigu.flink.chapter13;

import com.atguigu.flink.bean.UserBehavior;
import com.atguigu.flink.bean.WaterSensor;
import org.apache.flink.api.common.eventtime.WatermarkStrategy;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.shaded.guava18.com.google.common.hash.BloomFilter;
import org.apache.flink.shaded.guava18.com.google.common.hash.Funnel;
import org.apache.flink.shaded.guava18.com.google.common.hash.Funnels;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.windowing.ProcessWindowFunction;
import org.apache.flink.streaming.api.windowing.assigners.TumblingEventTimeWindows;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.streaming.api.windowing.windows.TimeWindow;
import org.apache.flink.util.Collector;

import java.time.Duration;

/**
 * @Author lizhenchao@atguigu.cn
 * @Date 2021/12/18 15:48
 */
public class Flink02_High_Project_UV {
    public static void main(String[] args) {
        Configuration conf = new Configuration();
        conf.setInteger("rest.port", 20000);
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment(conf);
        env.setParallelism(1);

        env
                .readTextFile("input/UserBehavior.csv")
                .map(line->{
                    String[] data = line.split(",");
                    return new UserBehavior(
                            Long.valueOf(data[0]),
                            Long.valueOf(data[1]),
                            Integer.valueOf(data[2]),
                            data[3],
                            Long.parseLong(data[4])*1000
                    );
                })
                .assignTimestampsAndWatermarks(
                        WatermarkStrategy
                                .<UserBehavior>forBoundedOutOfOrderness(Duration.ofSeconds(3))
                        .withTimestampAssigner((ub,ts)->ub.getTimestamp())
                )
                .filter(ub->"pv".equals(ub.getBehavior()))
                .keyBy(UserBehavior::getBehavior)
                .window(TumblingEventTimeWindows.of(Time.minutes(30)))
                .process(new ProcessWindowFunction<UserBehavior, String, String, TimeWindow>() {
                    @Override
                    public void process(String s,
                                        Context ctx,
                                        Iterable<UserBehavior> elements,
                                        Collector<String> out) throws Exception {
                        //布隆过滤器的一些设置                                            数据规模，误差率
                        BloomFilter<Long> bf = BloomFilter.create(Funnels.longFunnel(), 1000000, 0.01);

                        long uv=0;
                        for (UserBehavior ub : elements) {
                            boolean isSuccess=bf.put(ub.getUserId());
                            if (isSuccess) {
                                uv++;
                            }
                        }
                        out.collect(ctx.window()+"         "+uv);
                    }
                })
                .print();



            try {
                env.execute();
            } catch (Exception e) {
                e.printStackTrace();
            }

    }
}
