package com.atguigu.flink.chapter07;


import com.atguigu.flink.util.WindowUtil;
import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.windowing.ProcessWindowFunction;
import org.apache.flink.streaming.api.windowing.windows.GlobalWindow;
import org.apache.flink.util.Collector;

import java.util.List;

/**
 * 滚动窗口的简单实现
 * */
public class Flink05_Window_Count_Sliding {
    public static void main(String[] args) throws Exception {
        Configuration conf = new Configuration();

        conf.setInteger("rest.port",20000);

        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment(conf);
        env.setParallelism(1);
        env
                .socketTextStream("hadoop102",9999)
                .flatMap(new FlatMapFunction<String, Tuple2<String,Integer>>() {
                    @Override
                    public void flatMap(String value,
                                        Collector<Tuple2<String, Integer>> out) throws Exception {
                        for (String word :value.split(" ")) {
                            out.collect(Tuple2.of(word,1));
                        }
                    }
                })
                .keyBy(t->t.f0)
                .countWindow(4,2)
                .process(new ProcessWindowFunction<Tuple2<String, Integer>, String, String, GlobalWindow>() {

                    @Override
                    public void process(String s, Context context, Iterable<Tuple2<String, Integer>> elements, Collector<String> out) throws Exception {
                        List<Tuple2<String, Integer>> list = WindowUtil.toList(elements);
                        out.collect(list.toString());
                    }

                         }

                )
                .print();

        env.execute();
    }
}
