package com.atguigu.flink.chapter07.state;

import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.common.state.ListState;
import org.apache.flink.api.common.state.ListStateDescriptor;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.runtime.checkpoint.Checkpoint;
import org.apache.flink.runtime.state.FunctionInitializationContext;
import org.apache.flink.runtime.state.FunctionSnapshotContext;
import org.apache.flink.streaming.api.checkpoint.CheckpointedFunction;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.ProcessFunction;
import org.apache.flink.util.Collector;

import java.util.ArrayList;
import java.util.stream.Stream;

public class Flink01_Operator_ListState {
    public static void main(String[] args) throws Exception {
        Configuration conf = new Configuration();
        conf.setInteger("rest.port", 20000);
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment(conf);
        env.setParallelism(2);

        env.enableCheckpointing(3000);

        SingleOutputStreamOperator<String> stream = env
                .socketTextStream("hadoop102", 9999)
                .flatMap(new MyflatMapFunction());

        stream.print();

        stream
                .process(new ProcessFunction<String, String>() {
                    @Override
                    public void processElement(String value,
                                               Context ctx,
                                               Collector<String> out) throws Exception {
                        if (value.contains("x")){
                            throw new RuntimeException("故意制作的一个异常");
                        }
                    }
                })
                .print();


        env.execute();
    }

    public static class MyflatMapFunction implements FlatMapFunction<String, String>, CheckpointedFunction {

        ArrayList<String> list = new ArrayList<>();
        private ListState<String> wordState;

        @Override
        public void flatMap(String line,
                            Collector<String> out) throws Exception {
            String[] words = line.split(" ");
            for (String word : words) {
                list.add(word);
            }
            out.collect(list.toString());
        }

        //程序 重启时，从状态中恢复数据
        //一个并行度一次
        @Override
        public void snapshotState(FunctionSnapshotContext ctx) throws Exception {
            wordState.update(list);
        }

        @Override
        public void initializeState(FunctionInitializationContext ctx) throws Exception {
            wordState = ctx
                    .getOperatorStateStore()
                    .getUnionListState(new ListStateDescriptor<String>("wordState", String.class));
            Iterable<String> words = wordState.get();
            for (String word : words) {
                list.add(word);
            }
        }
    }
}
