package com.atguigu.flink.chapter07.state;

import com.atguigu.flink.bean.WaterSensor;
import org.apache.flink.api.common.state.*;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.datastream.BroadcastStream;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.KeyedProcessFunction;
import org.apache.flink.streaming.api.functions.co.BroadcastProcessFunction;
import org.apache.flink.util.Collector;

public class Flink04_Keyed_State_Value {
    public static void main(String[] args) throws Exception {
        Configuration conf = new Configuration();
        conf.setInteger("rest.port", 20000);
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment(conf);
        env.setParallelism(1);

        env
                .socketTextStream("hadoop102",9999)
                .map(line->{
                    String[] data = line.split(",");

                    return new WaterSensor(data[0],Long.valueOf(data[1]),Integer.valueOf(data[2]));
                })
                .keyBy(WaterSensor::getId)
                .process(new KeyedProcessFunction<String, WaterSensor, String>() {

                    private ValueState<Integer> lastVcState;

                    @Override
                    public void processElement(WaterSensor value,
                                               Context ctx,
                                               Collector<String> out) throws Exception {
                        //获取本次的水位值
                        Integer currentVc=value.getVc();
                        //从状态中获取上次数据的水位值
                        Integer lastVc = lastVcState.value();

                        if (lastVc!=null){
                            if (lastVc>10 && currentVc>10 ){
                                out.collect(ctx.getCurrentKey() + " 连续两次水位超过10 , 红色预警");                            }
                        }

                        lastVcState.update(currentVc);
                    }

                    @Override
                    public void open(Configuration parameters) throws Exception {
                        getRuntimeContext().getState(new ValueStateDescriptor<Integer>("lastVcState",Integer.class));

                    }

                })
                .print();

        env.execute();
    }


}
