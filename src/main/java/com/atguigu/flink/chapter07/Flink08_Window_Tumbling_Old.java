package com.atguigu.flink.chapter07;

import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.TimeCharacteristic;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.windowing.ProcessWindowFunction;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.streaming.api.windowing.windows.TimeWindow;
import org.apache.flink.util.Collector;

import java.util.ArrayList;
import java.util.Date;

/**

 */
public class Flink08_Window_Tumbling_Old {
    public static void main(String[] args) throws Exception {
        Configuration conf = new Configuration();
        conf.setInteger("rest.port", 20000);
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment(conf);
        env.setStreamTimeCharacteristic(TimeCharacteristic.ProcessingTime);
        
        env
            .socketTextStream("hadoop102", 9999)
            .flatMap(new FlatMapFunction<String, Tuple2<String, Long>>() {
                @Override
                public void flatMap(String value,
                                    Collector<Tuple2<String, Long>> out) throws Exception {
                    for (String word : value.split(" ")) {
                        out.collect(Tuple2.of(word, 1L));
                    
                    }
                
                }
            })
            .keyBy(t -> t.f0)
            .timeWindow(Time.seconds(5)) // 1.11 之前的旧写法
            .process(new ProcessWindowFunction<Tuple2<String, Long>, String, String, TimeWindow>() {

                @Override
                public void process(String key, Context ctx, Iterable<Tuple2<String, Long>> elements, Collector<String> out) throws Exception {

                        Date start = new Date(ctx.window().getStart());
                        Date end = new Date(ctx.window().getEnd());

                        ArrayList<String> words = new ArrayList<>();
                        elements.forEach(t -> words.add(t.f0));

                        out.collect("窗口:" + start + "  " + end + "  " + words);


                }
            })
            .print();
        
    
        env.execute();
    }
}
