package com.atguigu.flink.chapter07;


import com.atguigu.flink.bean.WaterSensor;
import org.apache.flink.api.common.functions.AggregateFunction;
import org.apache.flink.api.common.functions.ReduceFunction;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.windowing.WindowFunction;
import org.apache.flink.streaming.api.windowing.assigners.TumblingProcessingTimeWindows;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.streaming.api.windowing.windows.TimeWindow;
import org.apache.flink.util.Collector;

/**
 * 滚动窗口的简单实现
 */
public class Flink06_Window_ProcessFunction_2 {
    public static void main(String[] args) throws Exception {
        Configuration conf = new Configuration();

        conf.setInteger("rest.port", 20000);

        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment(conf);
        env.setParallelism(1);
        env
                .socketTextStream("hadoop102", 9999)
                .map(line -> {
                    String[] data = line.split(",");
                    return new WaterSensor(data[0], Long.valueOf(data[1]), Integer.valueOf(data[2]));
                })
                .keyBy(WaterSensor::getId)
                .window(TumblingProcessingTimeWindows.of(Time.seconds(5)))
                .aggregate(
                        new AggregateFunction<WaterSensor, MyAvg, Double>() {
                            @Override
                            public MyAvg createAccumulator() {
                                System.out.println("JOJO：老子不做人了");
                                return new MyAvg();
                            }

                            //累加器
                            @Override
                            public MyAvg add(WaterSensor value, MyAvg acc) {
                                System.out.println("累加！！他做了累加！！！");
                                acc.sum+=value.getVc();
                                acc.count++;
                                return acc;
                            }

                            //聚合最终结果
                            @Override
                            public Double getResult(MyAvg acc) {
                                System.out.println("聚合！！他做了聚合！！！");
                                return acc.avg();
                            }

                            //合并累加器
                            @Override
                            public MyAvg merge(MyAvg a, MyAvg b) {
                                System.out.println("合并！！他居然还想合并，想屁吃呢！！！");
                                return null;
                            }
                        }
                )
                .print();

        env.execute();
    }

    public static class MyAvg {
        public Integer sum = 0;
        public Long count = 0L;

        public Double avg() {
            return sum * 1.0 / count;
        }

    }
}
