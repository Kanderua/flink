package com.atguigu.flink.chapter07;

import com.atguigu.flink.bean.WaterSensor;
import com.atguigu.flink.util.WindowUtil;
import org.apache.flink.api.common.eventtime.*;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.windowing.ProcessWindowFunction;
import org.apache.flink.streaming.api.windowing.assigners.TumblingEventTimeWindows;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.streaming.api.windowing.windows.TimeWindow;
import org.apache.flink.util.Collector;

import java.time.Duration;
import java.util.List;

/**
 *
 */
public class Flink10_Watermark_Transform {
    public static void main(String[] args) throws Exception {
        Configuration conf = new Configuration();
        conf.setInteger("rest.port", 20000);
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment(conf);
        env.setParallelism(1);
        env.getConfig().setAutoWatermarkInterval(3000);
        env
            .socketTextStream("hadoop102", 9999)
            .map(line -> {
                String[] data = line.split(",");
                return new WaterSensor(data[0], Long.valueOf(data[1]), Integer.valueOf(data[2]));
            })
            .assignTimestampsAndWatermarks(
                WatermarkStrategy
                    .<WaterSensor>forBoundedOutOfOrderness(Duration.ofSeconds(3))
                    .withTimestampAssigner((ws, ts) -> ws.getTs())
            //当数据倾斜时水映不更新时,表示某个并行水映超过x秒没有更新则
                    .withIdleness(Duration.ofSeconds(5))
            )
            .keyBy(WaterSensor::getId)
            .window(TumblingEventTimeWindows.of(Time.seconds(5)))
            .process(new ProcessWindowFunction<WaterSensor, String, String, TimeWindow>() {
                @Override
                public void process(String key, Context ctx, Iterable<WaterSensor> elements, Collector<String> out) throws Exception {
                    List<WaterSensor> list = WindowUtil.toList(elements);
                    out.collect(key + "  " + elements);
                }


            })
            .print();
        
        env.execute();
    }

    public static class MyWaterMarkGenerator implements WatermarkGenerator<WaterSensor> {

        long maxTs = Long.MIN_VALUE + 3000;

        // 每来条数据执行一次
        @Override
        public void onEvent(WaterSensor event, long eventTimestamp, WatermarkOutput output) {
            maxTs = Math.max(maxTs, eventTimestamp);
            System.out.println("MyWaterMarkGenerator.onEvent");
            output.emitWatermark(new Watermark(maxTs - 3000));
        }

        // 周期性的执行
        @Override
        public void onPeriodicEmit(WatermarkOutput output) {
            System.out.println("MyWaterMarkGenerator.onPeriodicEmit");
        }
    }
}
