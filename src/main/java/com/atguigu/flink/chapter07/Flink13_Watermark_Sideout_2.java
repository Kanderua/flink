package com.atguigu.flink.chapter07;

import com.atguigu.flink.bean.WaterSensor;
import com.atguigu.flink.util.WindowUtil;
import org.apache.flink.api.common.eventtime.WatermarkStrategy;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.ProcessFunction;
import org.apache.flink.streaming.api.functions.windowing.ProcessWindowFunction;
import org.apache.flink.streaming.api.windowing.assigners.TumblingEventTimeWindows;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.streaming.api.windowing.windows.TimeWindow;
import org.apache.flink.util.Collector;
import org.apache.flink.util.OutputTag;

import java.time.Duration;
import java.util.List;

/**
 *
 */
public class Flink13_Watermark_Sideout_2 {
    public static void main(String[] args) throws Exception {
        Configuration conf = new Configuration();
        conf.setInteger("rest.port", 20000);
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment(conf);
        env.setParallelism(1);
        SingleOutputStreamOperator<String> mainStream = env
                .socketTextStream("hadoop102", 9999)
                .map(line -> {
                    String[] data = line.split(",");
                    return new WaterSensor(data[0], Long.valueOf(data[1]), Integer.valueOf(data[2]));
                })
                .process(new ProcessFunction<WaterSensor, String>() {
                    @Override
                    public void processElement(WaterSensor value,
                                               Context ctx,
                                               Collector<String> out) throws Exception {
                        //侧输出流的代码，sensor_1放主流，sensor_2放侧输出流
                        if ("sensor_1".equals(value.getId())){
                            out.collect(value.toString());
                        }else if ("sensor_2".equals(value.getId())) {
                            ctx.output(new OutputTag<String>("s2"){},value.toString());
                        }
                    }
                });
        mainStream.print("s_one");
        mainStream.getSideOutput(new OutputTag<WaterSensor>("s2"){}).print("s2");
        
        env.execute();
    }

}
