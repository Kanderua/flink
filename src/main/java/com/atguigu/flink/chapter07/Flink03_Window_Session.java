package com.atguigu.flink.chapter07;

import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.windowing.ProcessWindowFunction;
import org.apache.flink.streaming.api.windowing.assigners.ProcessingTimeSessionWindows;
import org.apache.flink.streaming.api.windowing.assigners.SessionWindowTimeGapExtractor;
import org.apache.flink.streaming.api.windowing.assigners.SlidingProcessingTimeWindows;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.streaming.api.windowing.windows.TimeWindow;
import org.apache.flink.util.Collector;

import java.util.ArrayList;
import java.util.Date;
import java.util.Random;

/**
 * 会话窗口
 *
 *  */
public class Flink03_Window_Session {
    public static void main(String[] args) throws Exception {
        Configuration conf = new Configuration();

        conf.setInteger("rest.port",20000);
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        //为保证会话数据都处于同一窗口，将并行度设置为1
        env.setParallelism(1);
        env
                .socketTextStream("hadoop102",9999)
                .flatMap(new FlatMapFunction<String, Tuple2<String,Integer>>() {
                    @Override
                    public void flatMap(String value,
                                        Collector<Tuple2<String, Integer>> out) throws Exception {
                        for (String word :value.split(" ")) {
                            out.collect(Tuple2.of(word,1));
                        }
                    }
                })
                .keyBy(t->t.f0)
                .window(ProcessingTimeSessionWindows.withDynamicGap(new SessionWindowTimeGapExtractor<Tuple2<String, Integer>>() {
                    @Override
                    public long extract(Tuple2<String, Integer> element) {
                        return new Random().nextInt(5000);
                    }
                }))
                .process(new ProcessWindowFunction<Tuple2<String, Integer>, String, String, TimeWindow>() {
                             @Override
                             //当时间大等于窗口结束时间时，关闭窗口，触发计算。
                             //在此处结束时间（5000ms内没有会话则视为会话结束）为window中设置的5000ms，
                             public void process(String key,
                                                 Context ctx,
                                                 Iterable<Tuple2<String, Integer>> elements,
                                                 Collector<String> out) throws Exception {
                                 Date start = new Date(ctx.window().getStart());
                                 Date end = new Date(ctx.window().getEnd());
                                 //存储窗口内的数据
                                 ArrayList<String> words = new ArrayList<>();
                                 elements.forEach(t->words.add(t.f0));

                                 out.collect("窗口:" + start + "  " + end + "  " + words);

                             }
                         }

                )
                .print();



        env.execute();
    }
}