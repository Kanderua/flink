package com.atguigu.flink.chapter09;

import com.atguigu.flink.bean.WaterSensor;
import org.apache.flink.api.common.eventtime.WatermarkStrategy;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.cep.CEP;
import org.apache.flink.cep.PatternSelectFunction;
import org.apache.flink.cep.PatternStream;
import org.apache.flink.cep.PatternTimeoutFunction;
import org.apache.flink.cep.pattern.Pattern;
import org.apache.flink.cep.pattern.conditions.SimpleCondition;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.util.OutputTag;

import java.time.Duration;
import java.util.List;
import java.util.Map;

/**
 *
 * @author yj2333
 */

public class Flink09_CEP_WithIn {
    public static void main(String[] args) {
        // 1. 先创建一个流
        Configuration conf = new Configuration();
        conf.setInteger("rest.port", 20000);
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment(conf);
        env.setParallelism(1);

        SingleOutputStreamOperator<WaterSensor> waterSensorStream = env
                .readTextFile("input/sensor.txt")
                .map(new MapFunction<String, WaterSensor>() {
                    @Override
                    public WaterSensor map(String value) throws Exception {
                        String[] split = value.split(",");
                        return new WaterSensor(split[0],
                                Long.parseLong(split[1]),
                                Integer.parseInt(split[2]));
                    }
                })
                .assignTimestampsAndWatermarks(
                        WatermarkStrategy
                                .<WaterSensor>forBoundedOutOfOrderness(Duration.ofSeconds(5))
                                .withTimestampAssigner((element, recordTimestamp) -> element.getTs())
                );

        // 2. 定义模式模板
        Pattern<WaterSensor, WaterSensor> pattern = Pattern
                .begin(
                        Pattern
                                .<WaterSensor>begin("s1")
                                .where(new SimpleCondition<WaterSensor>() {
                                    @Override
                                    public boolean filter(WaterSensor value) throws Exception {
                                        return "sensor_1".equals(value.getId());
                                    }
                                })
                                .next("s2")
                                .where(new SimpleCondition<WaterSensor>() {
                                    @Override
                                    public boolean filter(WaterSensor value) throws Exception {
                                        return "sensor_2".equals(value.getId());
                                    }
                                })
                ).times(2).consecutive();

        // 3. 将模式模板作用到流上
        PatternStream<WaterSensor> ps = CEP.pattern(waterSensorStream, pattern);

        // 4. 从模式流中获取匹配到的数据
        SingleOutputStreamOperator<String> normal = ps
                .select(
                        new OutputTag<String>("timeout") {},
                        new PatternTimeoutFunction<WaterSensor, String>() {
                            @Override
                            public String timeout(Map<String, List<WaterSensor>> map, long l) throws Exception {
                                // 返回值自动放入侧输出流
                                return map.toString();
                            }
                        },
                        new PatternSelectFunction<WaterSensor, String>() {
                            @Override
                            public String select(Map<String, List<WaterSensor>> map) throws Exception {
                                return map.toString();
                            }
                        });

        normal.print("normal");

        normal.getSideOutput(new OutputTag<String>("timeout"){}).print("timeout");

        try {
            env.execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
