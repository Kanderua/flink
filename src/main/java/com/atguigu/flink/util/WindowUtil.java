package com.atguigu.flink.util;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author lizhenchao@atguigu.cn
 * @Date 2021/12/15 10:18
 */
public class WindowUtil {
    public static <T> List<T> toList(Iterable<T> it){
        List<T> list = new ArrayList<>();
        for (T t : it) {
            list.add(t);
        }
    
        return list;
    }
}
