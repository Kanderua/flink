package com.atguigu.flink.chapter01_06.day2;

import org.apache.flink.api.common.functions.FilterFunction;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
/**
 *
 * 使用filter算子将流中的偶数全部取出，奇数全部丢弃。
 * */
public class Flink04_FilterFunction {
    public static void main(String[] args) throws Exception {
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);

        DataStreamSource<Integer> ds = env.fromElements(1, 2, 3, 4);

        ds.filter(new FilterFunction<Integer>() {
            @Override
            public boolean filter(Integer value) throws Exception {
                if (value%2==0){
                    return true;
                }
                return false;
            }
        }).print();

        env.execute("Flink04_FilterFunction");
    }
}