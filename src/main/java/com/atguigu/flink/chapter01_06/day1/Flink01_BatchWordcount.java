package com.atguigu.flink.chapter01_06.day1;

import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.java.ExecutionEnvironment;
import org.apache.flink.api.java.operators.*;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.util.Collector;

public class Flink01_BatchWordcount {
    public static void main(String[] args) throws Exception {
        ExecutionEnvironment env = ExecutionEnvironment.getExecutionEnvironment();

        DataSource<String> lineDS = env.readTextFile("input/date.txt");

        lineDS
                .flatMap(new FlatMapFunction<String, String>() {
                    @Override
                    public void flatMap(String line, Collector<String> out) throws Exception {
                        //value.
                        for (String word: line.split(" ")) {
                            out.collect(word);
                        }
                    }

                })
                .map(new MapFunction<String, Tuple2<String,Integer>>() {

                    @Override
                    public Tuple2<String, Integer> map(String value) throws Exception {

                        return Tuple2.of(value,1);
                    }
                })
                .groupBy(0).sum(1).print();

    }
}
