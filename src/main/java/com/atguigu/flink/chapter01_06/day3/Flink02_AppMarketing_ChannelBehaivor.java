package com.atguigu.flink.chapter01_06.day3;

import com.atguigu.flink.bean.MarketingUserBehavior;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

public class Flink02_AppMarketing_ChannelBehaivor {
    public static void main(String[] args) {
        Configuration conf = new Configuration();
        conf.setInteger("rest:port",10000);

        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);

        env.addSource(new MockAppDataSource())
                .map(new MapFunction<MarketingUserBehavior, Tuple2<String,Integer>>() {
                    @Override
                    public Tuple2 map(MarketingUserBehavior value) throws Exception {
                        return Tuple2.of(value.getChannel() + "\t" + value.getBehavior(), 1);
                    }
                })
                .keyBy(value -> value.f0).sum(1).print();
    }
}