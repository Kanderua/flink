package com.atguigu.flink.chapter11;

import com.atguigu.flink.bean.WaterSensor;
import org.apache.flink.api.common.eventtime.WatermarkStrategy;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.table.api.Expressions;
import org.apache.flink.table.api.Over;
import org.apache.flink.table.api.OverWindow;
import org.apache.flink.table.api.Table;
import org.apache.flink.table.api.bridge.java.StreamTableEnvironment;
import org.apache.flink.table.expressions.Expression;

import java.time.Duration;

import static org.apache.flink.table.api.Expressions.*;

/**
 * 将一串数字转换成一
 *
 * @author yj2333
 */
public class Flink11_Window_Over {
    public static void main(String[] args) throws Exception {
        Configuration conf = new Configuration();
        conf.setInteger("rest.port", 20000);

        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment(conf);
        env.setParallelism(1);

        DataStream<WaterSensor> waterSensorStream =
                env
                        .fromElements(new WaterSensor("sensor_1", 1001L, 10),
                                new WaterSensor("sensor_1", 2000L, 20),
                                new WaterSensor("sensor_2", 3000L, 30),
                                new WaterSensor("sensor_1", 4000L, 40),
                                new WaterSensor("sensor_1", 5000L, 50),
                                new WaterSensor("sensor_2", 6000L, 60))
                        //添加水印
                        .assignTimestampsAndWatermarks(
                                WatermarkStrategy
                                        .<WaterSensor>forBoundedOutOfOrderness(Duration.ofSeconds(3))
                                        .withTimestampAssigner((ws, ts) -> ws.getTs())
                        );
        // 1. 获取一个表的环境
        StreamTableEnvironment tEnv = StreamTableEnvironment.create(env);

        Table table = tEnv.fromDataStream(waterSensorStream, $("id"), $("ts").rowtime(), $("vc"));


        // 开窗函数的一般结构
        // sum(vc) over(partition by id order by ts rows between unbounded preceding and current row)

        // 无边界
        OverWindow w = Over.partitionBy($("id")).orderBy($("ts")).preceding(Expressions.UNBOUNDED_ROW).following(Expressions.CURRENT_ROW).as("w");

        //累加上n个，n = rowInterval(1L)
        //OverWindow w = Over.partitionBy($("id")).orderBy($("ts")).preceding(rowInterval(1L)).following(Expressions.CURRENT_ROW).as("w");


        //数据到达时间不同时和上无边界一样，有到达时间一样的情况时会把数据放到同一个窗口一起进行计算。
        //OverWindow w = Over.partitionBy($("id")).orderBy($("ts")).preceding(UNBOUNDED_RANGE).following(CURRENT_RANGE).as("w");

        //数据到达时间不同时和上无边界一样，有到达时间一样的情况时会把数据放到同一个窗口一起进行计算,这里是窗口向前一秒到达的数据进行聚合。
        //OverWindow w = Over.partitionBy($("id")).orderBy($("ts")).preceding(lit(1).second()).following(CURRENT_RANGE).as("w");


        table
                .window(w)
                .select($("id"), $("ts"), $("vc") ,$("vc").sum().over($("w")).as("sum_vc"))
                .execute()
                .print();

    }
}
