package com.atguigu.flink.chapter11;

import com.atguigu.flink.bean.WaterSensor;
import org.apache.flink.api.common.eventtime.WatermarkStrategy;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.table.api.Table;
import org.apache.flink.table.api.bridge.java.StreamTableEnvironment;

import java.time.Duration;

import static org.apache.flink.table.api.Expressions.$;

/**
 * 将一串数字转换成一
 * @author yj2333
 */
public class Flink10_Window_Grouped_SQL {
    public static void main(String[] args) throws Exception {
        Configuration conf = new Configuration();
        conf.setInteger("rest.port",20000);

        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment(conf);
        env.setParallelism(1);

        DataStream<WaterSensor> waterSensorStream =
                env
                        .fromElements(new WaterSensor("sensor_1", 1001L, 10),
                                new WaterSensor("sensor_1", 2000L, 20),
                                new WaterSensor("sensor_2", 3000L, 30),
                                new WaterSensor("sensor_1", 4000L, 40),
                                new WaterSensor("sensor_1", 5000L, 50),
                                new WaterSensor("sensor_2", 6000L, 60))
                        //添加水印
                        .assignTimestampsAndWatermarks(
                                WatermarkStrategy
                                        .<WaterSensor>forBoundedOutOfOrderness(Duration.ofSeconds(3))
                                        .withTimestampAssigner((ws, ts) -> ws.getTs())
                        );
        // 1. 获取一个表的环境
        StreamTableEnvironment tEnv = StreamTableEnvironment.create(env);

        Table table = tEnv.fromDataStream(waterSensorStream, $("id"), $("ts").rowtime(), $("vc"));

        //创建临时视图
        tEnv.createTemporaryView("sensor",table);


        /*
         * 三者的区别在于分组时调用的函数不同，具体参考
         * https://nightlies.apache.org/flink/flink-docs-release-1.14/zh/docs/dev/table/sql/queries/window-agg/
         *
         * */


        //滚动窗口
        tEnv.sqlQuery("select " +
                        "   id," +
                        "   tumble_start(ts, interval '5' second) stt," +
                        "   tumble_end(ts,interval '5' second) edt," +
                        "   sum(vc) sum_vc" +
                        "   from sensor" +
                        "   group by id ,TUMBLE(ts,interval '5' second)")
                .execute()
                .print();


        //滑动窗口
        tEnv.sqlQuery("select " +
                " id, " +
                " hop_start(ts, interval '2' second, interval '5' second) stt, " +
                " hop_end(ts, interval '2' second, interval '5' second) edt, " +
                " sum(vc) sum_vc " +
                "from sensor " +
                "group by id, hop(ts, interval '2' second, interval '5' second)")
                .execute()
                .print();


        //滑动窗口
        tEnv.sqlQuery("select " +
                " id, " +
                " session_start(ts, interval '2' second) stt, " +
                " session_end(ts, interval '2' second) edt, " +
                " sum(vc) sum_vc " +
                "from sensor " +
                "group by id, session(ts, interval '2' second)")
                .execute()
                .print();


    }
}
