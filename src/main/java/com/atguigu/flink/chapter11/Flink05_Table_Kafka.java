package com.atguigu.flink.chapter11;

import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.table.api.DataTypes;
import org.apache.flink.table.api.Table;
import org.apache.flink.table.api.bridge.java.StreamTableEnvironment;
import org.apache.flink.table.descriptors.*;

import static org.apache.flink.table.api.Expressions.$;

/**
 * 将一串数字转换成一
 * @author yj2333
 */
public class Flink05_Table_Kafka {
    public static void main(String[] args) throws Exception {
        Configuration conf = new Configuration();
        conf.setInteger("rest.port",20000);

        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment(conf);
        env.setParallelism(1);

        // 1. 获取一个表的环境
        StreamTableEnvironment tEnv = StreamTableEnvironment.create(env);

        // 2. 直接从文件中读取文件，使用tableAPI
        Schema schema = new Schema()
                .field("id", DataTypes.STRING())
                .field("ts", DataTypes.BIGINT())
                .field("vc", DataTypes.INT());

        tEnv.connect(
                //kafka相关配置设置
                new Kafka()
                .version("universal")
                .property("bootstrap.servers","hadoop162:9092")
                .property("group.id","atguigu")
                .topic("s1")
                .startFromLatest()
        )
                //设置解析的文本格式
                //.withFormat(new Csv())
                .withFormat(new Json())
                .withSchema(schema)
                .createTemporaryTable("sensor");

        Table sensor = tEnv.from("sensor");
        //sensor.execute().print();

        //按照特定逻辑进行查询
        Table result = sensor
                .where($("id").isEqual("sensor_1"))
                .select("*");

        //将查询的结果输出到Kafka的消费者中
        tEnv
                .connect(
                        new Kafka()
                        .version("universal")
                        .property("bootstrap.servers","hadoop162:9092")
                        .topic("s2")
                        .sinkPartitionerRoundRobin()
                )
                .withFormat(new Json())
                .withSchema(schema)
                .createTemporaryTable("s2");
        result.executeInsert("s2");

    }
}
