package com.atguigu.flink.chapter11;

import com.atguigu.flink.bean.WaterSensor;
import org.apache.flink.api.common.eventtime.WatermarkStrategy;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.table.api.Table;
import org.apache.flink.table.api.bridge.java.StreamTableEnvironment;

import java.time.Duration;
import java.time.ZoneOffset;

import static org.apache.flink.table.api.Expressions.$;

/**
 * 将一串数字转换成一
 * @author yj2333
 */
public class Flink09_Time_Event_SQL {
    public static void main(String[] args) throws Exception {
        Configuration conf = new Configuration();
        conf.setInteger("rest.port",20000);

        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment(conf);
        env.setParallelism(1);

        // 1. 获取一个表的环境
        StreamTableEnvironment tEnv = StreamTableEnvironment.create(env);

        //修改时区，从东八区改为零时区
        tEnv.getConfig().setLocalTimeZone(ZoneOffset.ofHours(0));

        tEnv.executeSql("create table sensor (" +
                "   id String, " +
                "   ts bigint, " +
                "   vc int, " +
                "   et as TO_TIMESTAMP_LTZ(ts,3), " +
                "   watermark for et as et - interval '3' second " +
                ")with(" +
                //一些相关配置
                "   'connector'='filesystem', " +
                "   'path'= 'input/sensor.txt', " +
                "   'format'= 'csv'" +
                ")"
        );

        tEnv.from("sensor").printSchema();

        tEnv.sqlQuery("select * from sensor").execute().print();
    }
}
