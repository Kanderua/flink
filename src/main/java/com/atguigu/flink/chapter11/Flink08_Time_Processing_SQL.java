package com.atguigu.flink.chapter11;

import com.atguigu.flink.bean.WaterSensor;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.table.api.Table;
import org.apache.flink.table.api.bridge.java.StreamTableEnvironment;

import static org.apache.flink.table.api.Expressions.$;

/**
 * 将一串数字转换成一
 * @author yj2333
 */
public class Flink08_Time_Processing_SQL {
    public static void main(String[] args) throws Exception {
        Configuration conf = new Configuration();
        conf.setInteger("rest.port",20000);

        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment(conf);
        env.setParallelism(1);

        // 1. 获取一个表的环境
        StreamTableEnvironment tEnv = StreamTableEnvironment.create(env);

        tEnv.executeSql("create table sensor(" +
                "   id string, " +
                "   ts bigint, " +
                "   vc int, " +
                "   pt as proctime() " +
                ")with(" +
                "   'connector'='filesystem', " +
                "   'path'='input/sensor.txt', " +
                "   'format'='csv' " +
                ")");
        //输出某张表
        tEnv.from("sensor").printSchema();

        tEnv.sqlQuery("select * from sensor").execute().print();
    }
}
