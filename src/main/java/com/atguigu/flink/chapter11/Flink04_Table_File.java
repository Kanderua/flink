package com.atguigu.flink.chapter11;

import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.table.api.DataTypes;
import org.apache.flink.table.api.Table;
import org.apache.flink.table.api.bridge.java.StreamTableEnvironment;
import org.apache.flink.table.descriptors.Csv;
import org.apache.flink.table.descriptors.FileSystem;
import org.apache.flink.table.descriptors.Schema;
import static org.apache.flink.table.api.Expressions.$;

/**
 * 将一串数字转换成一
 * @author yj2333
 */
public class Flink04_Table_File {
    public static void main(String[] args) throws Exception {
        Configuration conf = new Configuration();
        conf.setInteger("rest.port",20000);

        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment(conf);
        env.setParallelism(1);

        // 1. 获取一个表的环境
        StreamTableEnvironment tEnv = StreamTableEnvironment.create(env);

        // 2. 直接从文件中读取文件，使用tableAPI
        Schema schema = new Schema()
                .field("id", DataTypes.STRING())
                .field("ts", DataTypes.BIGINT())
                .field("vc", DataTypes.INT());

        //创建一个临时动态表和文件关联
        tEnv
                //从哪里读取数据
                .connect(new FileSystem().path("input/sensor.txt"))
                //解析格式（txt，csv。。。），后面是每个元素间的分隔符、每行的分隔符
                .withFormat(new Csv().fieldDelimiter(',').lineDelimiter("\n"))
                //动态表的格式
                .withSchema(schema)
                //临时表的表名
                .createTemporaryTable("sensor");

        Table sensor = tEnv.from("sensor");

        //对结果进行解析
        Table result = sensor
                .where($("id").isEqual("sensor_1"))
                .select($("id"), $("vc"), $("ts"));

        //输出时的临时表
        Schema schemaOut = new Schema()
                .field("id", DataTypes.STRING())
                .field("vc", DataTypes.INT())
                .field("ts", DataTypes.BIGINT());

        tEnv
                .connect(new FileSystem().path("input/out"))
                .withFormat(new Csv())
                .withSchema(schemaOut)
                .createTemporaryTable("out1");

        //将结果插入到
        result.executeInsert("out1");
    }
}
