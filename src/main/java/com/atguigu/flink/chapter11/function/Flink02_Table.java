package com.atguigu.flink.chapter11.function;


import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.table.api.Table;
import org.apache.flink.table.api.bridge.java.StreamTableEnvironment;
import org.apache.flink.table.functions.TableFunction;

import static org.apache.flink.table.api.Expressions.$;
import static org.apache.flink.table.api.Expressions.call;


public class Flink02_Table {
    public static void main(String[] args) {
        Configuration conf = new Configuration();
        conf.setInteger("rest.port", 20000);
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment(conf);
        env.setParallelism(2);

        DataStreamSource<String> waterSensorStream =
                env.fromElements("hello world", "hello hello", "hello atguigu atguigu");
        StreamTableEnvironment tEnv = StreamTableEnvironment.create(env);
        Table table = tEnv.fromDataStream(waterSensorStream, $("line"));
        tEnv.createTemporaryView("s", table);

        // 1.在table API中使用
        // 1.1 内联的方式使用
        table
            .joinLateral(call(MySplit.class, $("line")))
//            .leftOuterJoinLateral(call(MySplit.class, $("line")))
            .select($("line"), $("word"), $("len"))
            .execute()
            .print();

        System.out.println("===========================");
        // 1.2 函数先注册, 再使用

        // 2. 在sql中使用
        tEnv.createTemporaryFunction("my_split", MySplit.class);
        tEnv.sqlQuery("select " +
                          " line," +
                          " word," +
                          " len " +
                          "from s " +
                          "join lateral table(my_split(line)) on true ")
            .execute()
            .print();

        //SQL中常见的两种内连接方式
        // select .. from a join b on a.id=b.id
        // select .. from a,b where a.id=b.id
 /*       tEnv.sqlQuery("select " +
                " line," +
                " word," +
                " len " +
                "from s " +
                ", lateral table(my_split(line)) ")
                .execute()
                .print();


        tEnv.sqlQuery("select " +
                          " line," +
                          " word," +
                          " len " +
                          "from s " +
                          "left join lateral table(my_split(line)) on true ")
            .execute()
            .print();*/

    }

   /* @FunctionHint(output = @DataTypeHint("row<word string, len int>"))
    public static class MySplit extends TableFunction<Row> {
        public void eval(String line){
            if (line.contains("atguigu")) {
                return;
            }

            for (String word : line.split(" ")) {
                collect(Row.of(word, word.length()));
            }
        }
    }*/

    public static class MySplit extends TableFunction<WordLen> {
        public void eval(String line) {
            for (String word : line.split(" ")) {
                collect(new WordLen(word, word.length()));
            }
        }
    }

    public static class WordLen {
        public WordLen() {
        }

        public WordLen(String word, int len) {
            this.word = word;
            this.len = len;
        }

        private String word;
        private Integer len;

        public String getWord() {
            return word;
        }

        public void setWord(String word) {
            this.word = word;
        }

        public Integer getLen() {
            return len;
        }

        public void setLen(Integer len) {
            this.len = len;
        }
    }

}
