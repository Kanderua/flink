package com.atguigu.flink.chapter11;

import com.atguigu.flink.bean.WaterSensor;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.table.api.Table;
import org.apache.flink.table.api.bridge.java.StreamTableEnvironment;
import org.apache.flink.types.Row;

import static org.apache.flink.table.api.Expressions.$;

/**
 * 将一份数据转换成表格数据，查询出其中的某些字段
 * @author yj2333
 */
public class Flink01_Table_BaseUse {
    public static void main(String[] args) {
        Configuration conf = new Configuration();
        conf.setInteger("rest.port",20000);

        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment(conf);
        env.setParallelism(1);

        DataStreamSource<WaterSensor> waterSensorDataStream = env.fromElements(
                new WaterSensor("sensor_1", 1000L, 10),
                new WaterSensor("sensor_1", 2000L, 20),
                new WaterSensor("sensor_2", 3000L, 30),
                new WaterSensor("sensor_1", 4000L, 40),
                new WaterSensor("sensor_1", 5000L, 50),
                new WaterSensor("sensor_2", 6000L, 60),
                new WaterSensor("sensor_3", 7000L, 70)
        );
        // 1. 获取一个表的环境
        StreamTableEnvironment tEnv = StreamTableEnvironment.create(env);

        // 2. 将流转换成一个动态表，表的列名默认就是属性名
        Table table = tEnv.fromDataStream(waterSensorDataStream);

        // 3. 在动态表上执行连续查询, 得到一个新的动态表  select .. from t where id=''

        /*Table result = table
            .where("id='sensor_1'")
            .select("id,vc");*/

        Table result = table
                .where($("id").isEqual("sensor_1"))
                .select($("id"), $("vc"));

        // 4. 把新的动态表转成流, 打印
        DataStream<Row> stream = tEnv.toAppendStream(result, Row.class);
        stream.print();
        try {
            env.execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
