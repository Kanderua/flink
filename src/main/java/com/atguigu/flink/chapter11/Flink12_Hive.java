package com.atguigu.flink.chapter11;

import com.atguigu.flink.bean.WaterSensor;
import org.apache.flink.api.common.eventtime.WatermarkStrategy;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.table.api.Table;
import org.apache.flink.table.api.bridge.java.StreamTableEnvironment;
import org.apache.flink.table.catalog.hive.HiveCatalog;

import java.time.Duration;

import static org.apache.flink.table.api.Expressions.$;

/**
 * 将一串数字转换成一
 *
 * @author yj2333
 */
public class Flink12_Hive {
    public static void main(String[] args) throws Exception {
        System.setProperty("HADOOP_USER_NAME", "atguigu");
        Configuration conf = new Configuration();
        conf.setInteger("rest.port", 20000);
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment(conf);
        env.setParallelism(1);

        StreamTableEnvironment tEnv = StreamTableEnvironment.create(env);

        DataStreamSource<WaterSensor> waterSensorStream =
                env.fromElements(new WaterSensor("sensor_1", 1000L, 10),
                        new WaterSensor("sensor_1", 2000L, 20),
                        new WaterSensor("sensor_2", 3000L, 30),
                        new WaterSensor("sensor_1", 4000L, 40),
                        new WaterSensor("sensor_1", 5000L, 50),
                        new WaterSensor("sensor_2", 6000L, 60));
        Table table = tEnv.fromDataStream(waterSensorStream);
        tEnv.createTemporaryView("sensor", table);

        // 1. 先创建 hiveCatalog
        HiveCatalog hive = new HiveCatalog("hive", "default", "input/");
        // 2. 注册 hiveCatalog
        tEnv.registerCatalog("hive", hive);
        tEnv.useCatalog("hive");// 设置默认的catalog
        tEnv.useDatabase("gmall");
        // 3. 使用hiveCatalog
        tEnv.sqlQuery("select * from stu").execute().print();

        tEnv.useCatalog("default_catalog");
        tEnv.sqlQuery("select * from sensor").execute().print();

    }
}
