package com.atguigu.flink.chapter11;

import com.atguigu.flink.bean.WaterSensor;
import org.apache.flink.api.common.eventtime.WatermarkStrategy;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.table.api.Expressions;
import org.apache.flink.table.api.Over;
import org.apache.flink.table.api.OverWindow;
import org.apache.flink.table.api.Table;
import org.apache.flink.table.api.bridge.java.StreamTableEnvironment;

import java.time.Duration;

import static org.apache.flink.table.api.Expressions.$;

/**
 * 将一串数字转换成一
 *
 * @author yj2333
 */
public class Flink11_Window_Over_SQL {
    public static void main(String[] args) throws Exception {
        Configuration conf = new Configuration();
        conf.setInteger("rest.port", 20000);

        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment(conf);
        env.setParallelism(1);

        DataStream<WaterSensor> waterSensorStream =
                env
                        .fromElements(new WaterSensor("sensor_1", 1001L, 10),
                                new WaterSensor("sensor_2", 3000L, 30),
                                new WaterSensor("sensor_1", 4000L, 20),
                                new WaterSensor("sensor_1", 4000L, 40),
                                new WaterSensor("sensor_1", 5000L, 50),
                                new WaterSensor("sensor_2", 6000L, 60))
                        //添加水印
                        .assignTimestampsAndWatermarks(
                                WatermarkStrategy
                                        .<WaterSensor>forBoundedOutOfOrderness(Duration.ofSeconds(3))
                                        .withTimestampAssigner((ws, ts) -> ws.getTs())
                        );
        // 1. 获取一个表的环境
        StreamTableEnvironment tEnv = StreamTableEnvironment.create(env);

        Table table = tEnv.fromDataStream(waterSensorStream, $("id"), $("ts").rowtime(), $("vc"));

        tEnv.createTemporaryView("sensor",table);

        tEnv.sqlQuery("select " +
                "    id, " +
                "    ts, " +
                "    vc, " +
                // 来一条累加一条
                //"   sum(vc) over(partition by id order by ts rows between unbounded preceding and current row) sum_vc" +

                //累加前一条
                //"   sum(vc) over(partition by id order by ts rows between 1 preceding and current row) sum_vc" +

                // range表示同一时间到达的数据会放到同一个窗口中进行计算
                //"   sum(vc) over(partition by id order by ts range between unbounded preceding and current row) sum_vc" +

                // range表示同一时间到达的数据会放到同一个窗口中进行计算，这里是计算前一秒到达的数据
                //" sum(vc) over(partition by id order by ts range between interval '1' second preceding  and current row) sum_vc, " +

                //窗口中数据的最大值和最小值,此处窗口数据只有前一秒的和当前的
                " max(vc) over(partition by id order by ts range between interval '1' second preceding  and current row) max_vc ," +
                " min(vc) over(partition by id order by ts range between interval '1' second preceding  and current row) min_vc " +
                "   from sensor")
                .execute()
                .print();


        tEnv.sqlQuery("select" +
                " id, " +
                " ts, " +
                " vc, " +
                //这部分重复性代码可以进行封装成变量，方便代码的维护
                //                          " sum(vc) over(partition by id order by ts rows between unbounded preceding and current row) sum_vc " +
                //                          " sum(vc) over(partition by id order by ts rows between 1 preceding  and current row) sum_vc " +
                //                          " sum(vc) over(partition by id order by ts range between unbounded preceding  and current row) sum_vc " +
                " sum(vc) over w sum_vc, " +
                " max(vc) over w max_vc, " +
                " min(vc) over w min_vc " +
                "from sensor " +
                //代码被封装到此处
                "window w as(partition by id order by ts range between interval '1' second preceding  and current row)")
                .execute()
                .print();
    }
}
