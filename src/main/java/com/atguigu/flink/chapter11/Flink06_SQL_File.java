package com.atguigu.flink.chapter11;

import com.atguigu.flink.bean.WaterSensor;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.table.api.Table;
import org.apache.flink.table.api.bridge.java.StreamTableEnvironment;

/**
 * 将一串数字转换成一
 * @author yj2333
 */
public class Flink06_SQL_File {
    public static void main(String[] args) throws Exception {
        Configuration conf = new Configuration();
        conf.setInteger("rest.port",20000);

        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment(conf);
        env.setParallelism(1);

        // 1. 获取一个表的环境
        StreamTableEnvironment tEnv = StreamTableEnvironment.create(env);

        //建立一个表，与文件相关联
        tEnv.executeSql("create table sensor(" +
                "   id string, " +
                "   ts bigint, " +
                "   vc int" +
                //连接条件
                ")with(" +
                //连接器
                "   'connector'='filesystem', " +
                "   'path'='input/sensor.txt', " +
                "   'format'='csv' " +
                ")");

        Table table = tEnv.sqlQuery("select * from sensor where id='sensor_1'");

        // 动态表, 与sink关联（为了将table查到的数据写入）
        tEnv.executeSql("create table `out`(" +
                "   id string, " +
                "   ts bigint, " +
                "   vc int" +
                ")with(" +
                "   'connector'='filesystem', " +
                "   'path'='input/a.txt', " +
                "   'format'='csv' " +
                ")");

        table.executeInsert("out");
    }
}
