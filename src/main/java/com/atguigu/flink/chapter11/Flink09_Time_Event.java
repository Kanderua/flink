package com.atguigu.flink.chapter11;

import com.atguigu.flink.bean.WaterSensor;
import org.apache.flink.api.common.eventtime.WatermarkStrategy;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.table.api.Table;
import org.apache.flink.table.api.bridge.java.StreamTableEnvironment;

import java.time.Duration;

import static org.apache.flink.table.api.Expressions.$;

/**
 * 将一串数字转换成一
 * @author yj2333
 */
public class Flink09_Time_Event {
    public static void main(String[] args) throws Exception {
        Configuration conf = new Configuration();
        conf.setInteger("rest.port",20000);

        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment(conf);
        env.setParallelism(1);

        DataStream<WaterSensor> waterSensorStream =
                env
                        .fromElements(new WaterSensor("sensor_1", 1001L, 10),
                                new WaterSensor("sensor_1", 2000L, 20),
                                new WaterSensor("sensor_2", 3000L, 30),
                                new WaterSensor("sensor_1", 4000L, 40),
                                new WaterSensor("sensor_1", 5000L, 50),
                                new WaterSensor("sensor_2", 6000L, 60))
                        //添加水印
                        .assignTimestampsAndWatermarks(
                                WatermarkStrategy
                                        .<WaterSensor>forBoundedOutOfOrderness(Duration.ofSeconds(3))
                                        .withTimestampAssigner((ws, ts) -> ws.getTs())
                        );
        // 1. 获取一个表的环境
        StreamTableEnvironment tEnv = StreamTableEnvironment.create(env);

        //增加一个字段表示时间（取当前时间作为时间字段）
        Table table = tEnv.fromDataStream(waterSensorStream, $("id"), $("ts"), $("vc"), $("et").rowtime());


        //将现有的字段标记为时间
        //Table table = tEnv.fromDataStream(waterSensorStream, $("id"), $("ts").rowtime(), $("vc"));
        table.execute().print();

    }
}
