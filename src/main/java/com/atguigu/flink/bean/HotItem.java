package com.atguigu.flink.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**

 */
@NoArgsConstructor
@AllArgsConstructor
@Data
public class HotItem {
    private long itemId;
    private long wEnd;
    private Long count;
}
